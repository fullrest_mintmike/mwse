local modConfig = {}

local minWeightDefault = 5
local capacityDefault = 1000

modConfig.config = mwse.loadConfig("Duffel Bag config") or
{
	modEnable = true,
	takeBagAway = true,
	capacity = 1000,
	minWeight = 5,
}

local weightInp, capacityInp

function modConfig.onCreate(container)

	local pane = container:createThinBorder{}
	pane.widthProportional = 1.0
	pane.heightProportional = 1.0
	pane.paddingAllSides = 12
	pane.flowDirection = "top_to_bottom"

	local header = pane:createLabel{ text = "��������\n������ 0.1 �� Fullrest.ru" }
	header.color = tes3ui.getPalette("header_color")
	header.borderBottom = 25
	
 local descr = pane:createLabel{}
 descr.wrapText = true
 descr.height = 1
 descr.layoutWidthFraction = 1.0
 descr.layoutHeightFraction = -1.0
 descr.borderBottom = 25
 descr.text = "���������� ���� ��� ���� � �������� ����� ��� ������� < Backspace"
 
 local onoffBlock = pane:createBlock()
	onoffBlock.flowDirection = "left_to_right"
	onoffBlock.widthProportional = 1.0
	onoffBlock.autoHeight = true
	onoffBlock:createLabel({ text = "��������� ����" })
 
	local onoffButton = onoffBlock:createButton({ text = modConfig.config.modEnable and "�������" or "��������" })
	onoffButton.absolutePosAlignX = 1.0
	onoffButton.paddingTop = 2
	onoffButton.borderRight = 6
	onoffButton:register("mouseClick", function(e)
		modConfig.config.modEnable = not modConfig.config.modEnable
		if modConfig.config.modEnable then modConfig.onMod() else modConfig.offMod() end 
		onoffButton.text =  modConfig.config.modEnable and "�������" or "��������"
	end)
 
 local taBlock = pane:createBlock()
	taBlock.flowDirection = "left_to_right"
	taBlock.widthProportional = 1.0
	taBlock.autoHeight = true
	taBlock:createLabel({ text = "�������� ������ ����� � �����" })

	local taButton = taBlock:createButton({ text = modConfig.config.takeBagAway and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value })
	taButton.absolutePosAlignX = 1.0
	taButton.paddingTop = 2
	taButton.borderRight = 6
	taButton:register("mouseClick", function(e)
		modConfig.config.takeBagAway = not modConfig.config.takeBagAway
		taButton.text = modConfig.config.takeBagAway and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value
	end)

 local wBlock = pane:createBlock()
	wBlock.flowDirection = "left_to_right"
	wBlock.widthProportional = 1.0
	wBlock.maxHeight = 32
	wBlock.autoHeight = true
	wBlock:createLabel({ text = "������� �� ������ ���� ���� (������), ������� � ��������" })

 local wBoard = wBlock:createThinBorder{}
 wBoard.absolutePosAlignX = 1.0
	wBoard.borderRight = 8
	wBoard.maxHeight = 24
	wBoard.maxWidth = 80
	wBoard.paddingAllSides = 4
	weightInp = wBoard:createTextInput{}
	weightInp.text = tostring(modConfig.config.minWeight)
	weightInp.absolutePosAlignX = 0.5
	wBlock:register("mouseClick", function(e) tes3ui.acquireTextInput(weightInp)	end)
	
 local cBlock = pane:createBlock()
	cBlock.flowDirection = "left_to_right"
	cBlock.widthProportional = 1.0
	cBlock.maxHeight = 32
	cBlock.autoHeight = true
	cBlock:createLabel({ text = "����������� �������� �� ����" })

 local cBoard = cBlock:createThinBorder{}
 cBoard.absolutePosAlignX = 1.0
	cBoard.borderRight = 8
	cBoard.maxHeight = 24
	cBoard.maxWidth = 80
	cBoard.paddingAllSides = 4
	capacityInp = cBoard:createTextInput{}
	capacityInp.text = tostring(modConfig.config.capacity)
	capacityInp.absolutePosAlignX = 0.5
	cBlock:register("mouseClick", function(e) tes3ui.acquireTextInput(capacityInp)	end)
end

function modConfig.onClose(container)
  local i,j = string.find(weightInp.text, "%d+")
  if i then modConfig.config.minWeight = tonumber(string.sub(weightInp.text,i,j))
       else modConfig.config.minWeight = minWeightDefault
  end
  local i,j = string.find(capacityInp.text, "%d+")
  if i then modConfig.config.capacity = tonumber(string.sub(capacityInp.text,i,j))
       else modConfig.config.capacity = capacityDefault
  end
	 mwse.saveConfig("Duffel Bag config", modConfig.config, { indent = true })
end

return modConfig
