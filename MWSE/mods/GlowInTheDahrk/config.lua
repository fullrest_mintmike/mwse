local config = mwse.loadConfig("Glow in the Dahrk", {
	varianceInMinutes = 30,
	useVariance = false,
	addInteriorLights = true,
	addInteriorSunrays = true,
	translations = false,
})

-- Remove legacy values.
config.cellData = nil

return config