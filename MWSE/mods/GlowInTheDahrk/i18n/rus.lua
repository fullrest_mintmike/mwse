return {
	["logConvertingLegacyMesh"] = "[Glow In The Dahrk] Converting legacy mesh: %s",
	["logMalformedAssetSwitchNodeIsNotCorrectType"] = "[Glow In The Dahrk] ERROR: Encountered malformed mesh '%s'. NightDaySwitch is of type '%s' and not 'NiSwichNode'.",
	["mcm.addInteriorLights.description"] = "��� ���������, ���� � ���������� ����� ������� � ������� �����.\n\n�� ���������: ���",
	["mcm.addInteriorLights.label"] = "�������� ��������� ���������� ����, � ����������?",
	["mcm.addInteriorSunrays.description"] = "��� ���������, ��������� ���� ����� ��������� � ����� � ����������.\n\n�� ���������: ���",
	["mcm.addInteriorSunrays.label"] = "�������� ��������� ���� ��� ���� � ����������?",
	["mcm.info"] = "Glow in the Dahrk\n\n������ � �������� �� Melchior Dahrk\n������� �� NullCascade.\n",
	["mcm.useVariance.description"] = "������� ��������� ������� ���� �� �������. ��� ���������� ���� ����� ����������/������� ������������.\n\n�� ���������: ���",
	["mcm.useVariance.label"] = "������� ������� ������� �������������/��������� ��� ������ ������� ����?",
	["mcm.varianceInMinutes.description"] = "������� �� ������� � ������� �� �������/����� ������, ����� ���� �������� ����������/�������.\n\n�� ���������: 30",
	["mcm.varianceInMinutes.label"] = "������������ ������� (� �������)",
}